package org.newcih.service;

import org.springframework.stereotype.Service;

import java.io.File;

import static org.newcih.web.MainController.INPUTDATA_DIR_ROOT;

@Service
public class MainService {

    /**
     * 根据字体名，返回生产目录（即存放box文件和tif图片的目录）
     *
     * @param fontName
     * @return
     */
    public static String generatorFolder(String fontName) {
        String folderName = INPUTDATA_DIR_ROOT + fontName;
        File   folder     = new File(folderName);
        if (!folder.exists()) {
            folder.mkdirs();
        }

        return folderName;
    }

}
