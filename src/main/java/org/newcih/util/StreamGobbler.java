package org.newcih.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * When Runtime.exec() won't
 */
public class StreamGobbler extends Thread {

    InputStream is;
    StringBuilder outputMessage = new StringBuilder();

    private final static Logger logger = Logger.getLogger(StreamGobbler.class.getName());

    public StreamGobbler(InputStream is) {
        this.is = is;
    }

    public String getMessage() {
        return outputMessage.toString();
    }

    @Override
    public void run() {
        try {
            InputStreamReader isr            = new InputStreamReader(is, StandardCharsets.UTF_8);
            BufferedReader    bufferedReader = new BufferedReader(isr);
            String            line;
            while ((line = bufferedReader.readLine()) != null) {
                outputMessage.append(line).append("\n");
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
