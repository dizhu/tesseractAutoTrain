package org.newcih.pojo.dto;

import java.util.List;

/**
 * 字体集合
 *
 * @author newcih
 * @version 2018-02-06
 */
public class FontSet {

    /**
     * 字体集合
     */
    private List<String> fontList;
    /**
     * 总数
     */
    private int          totalCount;

    public List<String> getFontList() {
        return fontList;
    }

    public void setFontList(List<String> fontList) {
        this.fontList = fontList;
    }

    public int getTotalCount() {
        return fontList != null ? fontList.size() : 0;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
}
