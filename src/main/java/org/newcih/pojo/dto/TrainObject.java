package org.newcih.pojo.dto;

import java.io.Serializable;
import java.util.Map;

/**
 * 训练接口的传输对象
 *
 * @author newcih
 * @version 2018-02-05
 */
public class TrainObject implements Serializable {

    private static final long serialVersionUID = 4335569461533698852L;

    /**
     * 训练数据
     */
    private Map<String, String> trainData;
    /**
     * 新增字体名
     */
    private String              newFontName;
    /**
     * 基准字体名
     */
    private String              baseFontName;

    /**
     * Getter And Setter
     */
    public Map<String, String> getTrainData() {
        return trainData;
    }

    public void setTrainData(Map<String, String> trainData) {
        this.trainData = trainData;
    }

    public String getNewFontName() {
        return newFontName;
    }

    public void setNewFontName(String newFontName) {
        this.newFontName = newFontName;
    }

    public String getBaseFontName() {
        return baseFontName;
    }

    public void setBaseFontName(String baseFontName) {
        this.baseFontName = baseFontName;
    }
}
