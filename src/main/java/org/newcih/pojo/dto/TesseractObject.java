package org.newcih.pojo.dto;

/**
 * 识别接口的传输对象
 *
 * @author newcih
 * @version 2018-02-05
 */
public class TesseractObject {

    /**
     * 识别使用的字体名
     */
    private String fontName;
    /**
     * 图片的Base64编码数据
     */
    private String imageStr;

    /**
     * Getter And Setter
     */
    public String getFontName() {
        return fontName;
    }

    public void setFontName(String fontName) {
        this.fontName = fontName;
    }

    public String getImageStr() {
        return imageStr;
    }

    public void setImageStr(String imageStr) {
        this.imageStr = imageStr;
    }
}
