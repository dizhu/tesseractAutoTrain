package org.newcih.pojo;

import java.awt.*;
import java.io.File;

import static org.newcih.util.ImageUtils.TIF_FILENAME_SEPARATOR_CHAR;

/**
 * Box文件的行对象
 *
 * @author newcih
 * @version 2018-01-26
 */
public class BoxRow {

    /**
     * 值
     */
    private String    value;
    /**
     * 位置和图块大小
     */
    private Rectangle rectangle;
    /**
     * 在集合中的索引
     */
    private int       index;

    public BoxRow() {
        this.rectangle = new Rectangle();
        this.rectangle.x = 0;
        this.rectangle.y = 0;
    }

    @Override
    public String toString() {
        return "BoxRow{" + "value='" + value + '\'' + ", rectangle=" + rectangle + ", index=" + index + '}';
    }

    /**
     * 构造方法
     */
    public BoxRow(String value, int index) {
        this.value = value;
        this.index = index;
    }

    /**
     * 从图片文件的文件名中获取value
     *
     * @param image
     * @return
     */
    public static String getValue(File image) {
        if (image == null) {
            return null;
        }

        String fileName = image.getName();
        String value = fileName.substring(fileName.indexOf(TIF_FILENAME_SEPARATOR_CHAR) + 1,
                                          fileName.lastIndexOf(TIF_FILENAME_SEPARATOR_CHAR));
        return value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public void setRectangle(Rectangle rectangle) {
        this.rectangle = rectangle;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

}
