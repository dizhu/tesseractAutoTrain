package org.newcih.pojo;

import java.awt.image.BufferedImage;
import java.util.List;

/**
 * Box文件对象
 *
 * @author newcih
 * @version 2018-01-26
 */
public class Box {

    private List<BoxRow> boxRowList;

    public Box() {

    }

    public Box(BufferedImage image) {

    }

    public List<BoxRow> getBoxRowList() {
        return boxRowList;
    }

    public void setBoxRowList(List<BoxRow> boxRowList) {
        this.boxRowList = boxRowList;
    }
}
